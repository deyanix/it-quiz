import Vue from 'vue';
import VueRouter from 'vue-router';
import BootstrapVue from 'bootstrap-vue';
import { library as FontAwesome } from '@fortawesome/fontawesome-svg-core'
import { fas } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import QuizBrowser from "./pages/QuizBrowser/QuizBrowser.vue";
import GameView from "./pages/GameView/GameView.vue";
import AboutMe from "./pages/AboutMe/AboutMe.vue";

FontAwesome.add(fas);
Vue.component('font-awesome-icon', FontAwesomeIcon);
Vue.use(VueRouter);
Vue.use(BootstrapVue);

const router = new VueRouter({
    routes: [
        {
            path: '/',
            redirect: {
                name: 'quizBrowser'
            }
        },
        {
            name: 'quizBrowser',
            path: '/browser',
            component: QuizBrowser
        },
        {
            name: 'aboutMe',
            path: '/about-me',
            component: AboutMe
        },
        {
            name: 'quizGame',
            path: '/quiz/',
            component: GameView,
            children: [
                {
                    name: 'quizGameQuestion',
                    path: ':question'
                }
            ]
        }
    ]
});

new Vue({
    router: router,
    template: '<router-view></router-view>'
}).$mount('#app');


