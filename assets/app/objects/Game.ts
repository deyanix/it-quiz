import Quiz from "./Quiz";
import GameQuestion, {Answer} from "./GameQuestion";
import axios from "axios";
import * as Cookies from "es-cookie";

export default class Game {
    protected static prepareGame(rawGame: any): Game {
        let game = new Game(rawGame.token);
        game.startTime = new Date(rawGame.startTime);
        game.quiz = Quiz.loadByQid(rawGame.qid);

        let rawQuestions = rawGame.questions;
        for (let questionKit of Object.keys(rawQuestions).map(k => [k, rawQuestions[k]])) {
            let question = new GameQuestion(parseInt(questionKit[0]));
            let rawQuestion = questionKit[1];
            question.question = rawQuestion.content;
            question.image = rawQuestion.image;
            question.multiple = rawQuestion.type === "multiple";

            let rawAnswers = rawQuestion.answers;
            for (let answerKit of Object.keys(rawAnswers).map(k => [k, rawAnswers[k]])) {
                question.answers.push({gid: parseInt(answerKit[0]), content: answerKit[1]});
            }
            question.loadAnswer();
            game.gameQuestions.push(question);
        }
        return game;
    }

    public static createGame(quiz: Quiz, callback: (game: Game) => any = () => {}) {
        axios.get('rest/create/' + quiz.qid).then((response) => {
            let game = Game.prepareGame(response.data);
            Cookies.set("gameToken", game.token);
            Cookies.remove('gameAnswer');
            callback(game);
        });
    }

    public static loadGame(callback: (game: Game) => any = () => {}) {
        let token = Cookies.get("gameToken");
        if (token === undefined) {
            if (callback !== null) {
                callback(null);
            }
            return;
        }

        axios.get('rest/load/' + token).then((response) => {
            callback(Game.prepareGame(response.data));
        });
    }

    public static existsGame(callback: (exists: boolean, playable: boolean) => any = () => {}) {
        let token = Cookies.get("gameToken");
        if (token === undefined) {
            if (callback !== null) {
                callback(false, false);
            }
            return;
        }

        axios.get('rest/exists/' + token).then((response) => {
            callback(response.data.exists, response.data.playable);
        });
    }

    public removeGame(callback: () => any = () => {}) {
        axios.get('rest/delete/' + this.token).then((response) => {
            callback();
        });
    }

    public endGame(callback: (points: number) => any = () => {}) {
        let preparedQuestions = {};
        for (let question of this.gameQuestions) {
            let preparedQuestion = question.prepare();
            if (!!preparedQuestion) {
                preparedQuestions[question.gid] = preparedQuestion;
            }
        }
        axios.post('rest/check/' + this.token, {
            game_choice: preparedQuestions
        }).then((response) => {
            callback(response.data.points);
        });
     }

    private _token: string;
    private _quiz: Quiz;
    private _startTime: Date;
    private _gameQuestions: GameQuestion[] = [];
    private _stopped: boolean = false;

    constructor(token: string) {
        this._token = token;
    }

    findGameQuestionById(gid: number): GameQuestion {
        for (let gameQuestion of this._gameQuestions) {
            if (gameQuestion.gid == gid) {
                return gameQuestion;
            }
        }
        return null;
    }

    get token(): string {
        return this._token;
    }

    set token(value: string) {
        this._token = value;
    }

    get quiz(): Quiz {
        return this._quiz;
    }

    set quiz(value: Quiz) {
        this._quiz = value;
    }

    get startTime(): Date {
        return this._startTime;
    }

    set startTime(value: Date) {
        this._startTime = value;
    }

    get gameQuestions(): GameQuestion[] {
        return this._gameQuestions;
    }

    set gameQuestions(value: GameQuestion[]) {
        this._gameQuestions = value;
    }

    get stopped(): boolean {
        return this._stopped;
    }

    set stopped(value: boolean) {
        this._stopped = value;
    }
}