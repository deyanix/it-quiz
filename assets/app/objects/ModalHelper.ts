import {Vue} from 'vue-property-decorator';

export default class ModalHelper {
    private readonly _vueElement;
    private readonly _modalRef;

    constructor(vueElement: Vue, modalRef: string) {
        this._vueElement = vueElement;
        this._modalRef = modalRef;
    }

    show() {
        this._vueElement.$refs[this._modalRef].show();
    }

    hide() {
        this._vueElement.$refs[this._modalRef].hide();
    }

    get vueElement() {
        return this._vueElement;
    }

    get modalRef() {
        return this._modalRef;
    }
}