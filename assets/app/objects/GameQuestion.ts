import * as Cookies from "es-cookie";

type State = "idle"|"omitted"|"answered";
export type Answer = {gid: number, content: string};

export default class GameQuestion {
    private _gid: number;
    private _multiple: boolean;
    private _question: string;
    private _answers: Answer[] = [];
    private _image: string;
    private _choice: Answer[]|Answer;
    private _state: State;

    constructor(gid: number) {
        this._gid = gid;
        this._state = 'idle';
    }

    prepare() {
        let localChoice;
        if (this.state === "answered") {
            if (Array.isArray(this.choice)) {
                localChoice = [];
                for (let i = 0; i < this.choice.length; i++) {
                    localChoice[i] = this.choice[i].gid;
                }
            } else {
                localChoice = this.choice.gid;
            }
        }
        return localChoice;
    }

    private getStateId(): number {
        switch (this.state) {
            case "idle":
                return 0;
            case "omitted":
                return 1;
            case "answered":
                return 2;
        }
    }

    private setStateId(state: number) {
        switch (state) {
            case 0:
                this.state = "idle";
                break;
            case 1:
                this.state = "omitted";
                break;
            case 2:
                this.state = "answered";
                break;
        }
    }

    private pushAnswer() {
        let answers: any = Cookies.get('gameAnswer');
        if (!!!answers) {
            answers = {};
        } else {
            answers = JSON.parse(answers);
        }

        let localChoice = [];
        localChoice[0] = this.getStateId();
        console.log("omitted1");
        if (this.state === "answered") {
            console.log("omitted2");
            if (Array.isArray(this.choice)) {
                localChoice[1] = [];
                for (let i = 0; i < this.choice.length; i++) {
                    localChoice[1][i] = this.choice[i].gid;
                }
            } else {
                localChoice[1] = this.choice.gid;
            }
        }
        console.log("omitted3");
        answers[this.gid] = localChoice;
        console.log("omitted4", answers);
        Cookies.set("gameAnswer", JSON.stringify(answers));
    }

    loadAnswer() {
        let answers: any = Cookies.get('gameAnswer');
        if (!!!answers)  {
            return;
        }
        answers = JSON.parse(answers);


        if (!!answers[this.gid]) {
            let localChoice = answers[this.gid];
            this.setStateId(localChoice[0]);
            if (this.state === "answered") {
                if (Array.isArray(localChoice[1])) {
                    this.choice = [];
                    for (let i = 0; i < localChoice[1].length; i++) {
                        this.choice[i] = this.findAnswerByGid(localChoice[1][i]);
                    }
                } else {
                    this.choice = this.findAnswerByGid(localChoice[1]);
                }
            }
        }
    }

    findAnswerByGid(gid: number): Answer {
        for (let answer of this.answers) {
            if (answer.gid === gid) {
                return answer;
            }
        }
        return null;
    }

    chooseAnswer(answer: Answer[]|Answer) {
        this._choice = answer;
        this.state = "answered";
        this.pushAnswer();
    }

    skipQuestion() {
        this._choice = undefined;
        this.state = "omitted";
        this.pushAnswer();
    }

    get choice(): Answer[]|Answer {
        return this._choice;
    }

    set choice(choice: Answer[]|Answer) {
        this._choice = choice;
    }

    get state(): State {
        return this._state;
    }

    set state(value: State) {
        this._state = value;
    }

    get gid(): number {
        return this._gid;
    }

    set gid(value: number) {
        this._gid = value;
    }

    get multiple(): boolean {
        return this._multiple;
    }

    set multiple(value: boolean) {
        this._multiple = value;
    }

    get question(): string {
        return this._question;
    }

    set question(value: string) {
        this._question = value;
    }

    get answers(): Answer[] {
        return this._answers;
    }

    set answers(value: Answer[]) {
        this._answers = value;
    }

    get image(): string {
        return this._image;
    }

    set image(value: string) {
        this._image = value;
    }
}