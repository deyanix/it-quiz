import axios from 'axios';
import Game from "./Game";

export default class Quiz {
    private static _quizzes: Quiz[];

    public static init(callback: (quizzes: Quiz[]) => any = () => {}): void {
        if (Quiz._quizzes !== undefined) {
            callback(Quiz._quizzes);
            return;
        }
        axios.get('rest/quizzes').then((response) => {
            let quizzes = [];
            for (let quiz of response.data) {
                let quizObject = new Quiz(quiz.qid, quiz.name);
                quizObject.description = quiz.description;
                quizObject.questionsAmount = quiz.questionsAmount;
                quizObject.time = quiz.time;
                if (quiz.thumbnail) {
                    quizObject.thumbnail = "rest/thumbnail/" + quizObject.qid;
                }
                quizzes.push(quizObject);
            }
            Quiz._quizzes = quizzes;
            callback(quizzes);
        });
    }

    public static loadByQid(qid: number): Quiz {
        for (let quiz of Quiz._quizzes) {
            if (quiz.qid === qid) {
                return quiz;
            }
        }
        return null;
    }

    private _qid: number;
    private _name: string;
    private _description: string;
    private _time: number; //seconds
    private _thumbnail: string;
    private _questionsAmount: number;

    constructor(id: number, name: string) {
        this._qid = id;
        this._name = name;
    }

    get qid(): number {
        return this._qid;
    }

    set qid(value: number) {
        this._qid = value;
    }

    get name(): string {
        return this._name;
    }

    set name(value: string) {
        this._name = value;
    }

    get description(): string {
        return this._description;
    }

    set description(value: string) {
        this._description = value;
    }

    get time(): number {
        return this._time;
    }

    set time(value: number) {
        this._time = value;
    }

    get questionsAmount(): number {
        return this._questionsAmount;
    }

    set questionsAmount(value: number) {
        this._questionsAmount = value;
    }

    get thumbnail(): string {
        return this._thumbnail;
    }

    set thumbnail(value: string) {
        this._thumbnail = value;
    }
}