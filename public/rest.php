<?php

	use FastRoute\RouteCollector;
	use Quiz\GameQuiz;
	use Quiz\Quiz;
	require "../vendor/autoload.php";
	define('PROJECT_DIR', realpath(__DIR__."/.."));

	$klein = new \Klein\Klein();
	$klein->with('/projekt-szkolny/public/rest', function() use ($klein) {
		$klein->respond('GET', '/quizzes', function ($request, $response, $service) {
			$preparedQuizzes = [];
			foreach (Quiz::scanQuizzes() as $quiz) {
				$preparedQuizzes[] = $quiz->prepare();
			}
			$response->json($preparedQuizzes);
		});
		$klein->respond('GET', '/thumbnail/[:qid]', function ($request, $response, $service) {
			$path = Quiz::generatePathByQid($request->qid);
			if (is_file("$path/thumbnail.jpg")) {
				$response->file("$path/thumbnail.jpg", "thumbnail.jpg");
			}
		});
		$klein->respond('GET', '/image/[:qid]/[:image]', function ($request, $response, $service) {
			$path = Quiz::generatePathByQid($request->qid);
			if (is_file("$path/".$request->image)) {
				$response->file("$path/".$request->image, $request->image);
			}
		});
		$klein->respond('GET', '/create/[:qid]', function ($request, $response, $service) {
			$quiz = Quiz::loadQuiz($request->qid);
			$game = GameQuiz::createGame($quiz);
			$response->json($game->prepare());
		});
		$klein->respond('GET', '/load/[:token]', function ($request, $response, $service) {
			$game = GameQuiz::loadGame($request->token);
			if (empty($game)) {
				$response->json([]);
			} else {
				$response->json($game->prepare());
			}
		});
		$klein->respond('GET', '/delete/[:token]', function ($request, $response, $service) {
			GameQuiz::deleteGame($request->token);
			$response->json([]);
		});
		$klein->respond('GET', '/exists/[:token]', function ($request, $response, $service) {
			$game = GameQuiz::loadGame($request->token);
			if (empty($game)) {
				$status = false;
				$playable = false;
			} else {
				$status = true;
				$playable = ($game->getStartTime() + ($game->getQuiz()->getTime()*1000) > round(microtime(true)*1000));
			}
			$response->json(["exists" => $status, "playable" => $playable]);
		});
		$klein->respond(['POST', 'GET'], '/check/[:token]', function ($request, $response, $service) {
			$data = file_get_contents('php://input');
			if (!empty($data)) {
				$json = json_decode($data, true);
				$game = GameQuiz::loadGame($request->token);
				if (!empty($game) && isset($json['game_choice'])) {
					foreach ($json['game_choice'] as $gid => $choice) {
						if (!empty($choice)) {
							$game->findGameQuestionByGid(intval($gid))->setChoice($choice);
						}
					}
					GameQuiz::deleteGame($game->getToken());
					$response->json(["points" => $game->check()]);
				}
			} else {
				$response->json([]);
			}
		});
	});

	$klein->dispatch();
