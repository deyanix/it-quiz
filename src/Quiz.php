<?php
	namespace Quiz;

	class Quiz {
		public static function generatePathByQid(string $qid): ?string {
			$path = PROJECT_DIR."/quiz/$qid";
			if (is_dir($path)) {
				return $path;
			} else {
				return null;
			}
		}

		public static function scanQuizzes(): array {
			$path = PROJECT_DIR."/quiz/";
			if (!is_dir($path)) {
				mkdir($path);
				return [];
			}

			$quizzes = [];
			foreach (scandir($path) as $qid) {
				$metadata = "$path/$qid/quiz.xml";
				if (is_file($metadata)) {
					$quizzes[] = self::loadQuiz($qid);
				}
			}
			return $quizzes;
		}

		public static function loadQuiz(string $qid): ?Quiz {
			$path = self::generatePathByQid($qid);
			$filepath = $path."/quiz.xml";
			if (empty($path) || !is_file($filepath)) {
				return null;
			}
			$doc = new \DOMDocument();
			$doc->load($filepath);

			$quizObject = new Quiz($qid);
			$quizNode = $doc->documentElement;
			$quizHelper = new XMLHelper($quizNode);
			$quizObject->setName($quizHelper->findSingleElementValue('name'));
			$quizObject->setDescription($quizHelper->findSingleElementValue('description'));
			$quizObject->setTime($quizHelper->findSingleElementValue('time', 'integer'));
			$quizObject->setQuestionsAmount($quizHelper->findSingleElementValue('questionAmount', 'integer'));

			$questionsNode = $quizHelper->findSingleElement('questions');
			if (!empty($questionsNode)) {
				foreach ($questionsNode->getElementsByTagName('question') as $index => $questionNode) {
					if ($questionNode instanceof \DOMElement) {
						$quizObject->addQuestion(QuizQuestion::loadByElement($index+1, $questionNode));
					}
				}
			}

			return $quizObject;
		}

		private $qid;
		private $name;
		private $description;
		private $time;
		private $questionsAmount;
		private $questions = [];

		protected function __construct(string $qid) {
			$this->qid = $qid;
		}

		public function getQid(): string {
			return $this->qid;
		}

		public function getPath(): string {
			return self::generatePathByQid($this->qid);
		}

		public function getName(): ?string {
			return $this->name;
		}

		public function setName(?string $name): void {
			$this->name = $name;
		}

		public function getDescription(): ?string {
			return $this->description;
		}

		public function setDescription(?string $description): void {
			$this->description = $description;
		}

		public function getTime(): ?int {
			return $this->time;
		}

		public function setTime(?int $time): void {
			$this->time = $time;
		}

		public function getQuestionsAmount(): ?int {
			return $this->questionsAmount;
		}

		public function setQuestionsAmount(?int $questionsAmount): void {
			$this->questionsAmount = $questionsAmount;
		}

		public function addQuestion(QuizQuestion $question): void {
			$this->questions[] = $question;
		}

		public function getQuestions(): array {
			return $this->questions;
		}

		public function getQuestion(int $id): ?QuizQuestion {
			foreach ($this->questions as $question) {
				if ($question->getId() === $id) {
					return $question;
				}
			}
			return null;
		}

		public function setQuestions(?array $questions): void {
			$this->questions = $questions;
		}

		public function prepare(): array {
			$data["qid"] = $this->qid;
			$data["name"] = $this->name;
			$data["description"] = $this->description;
			$data["time"] = $this->time;
			$data["questionsAmount"] = $this->questionsAmount;
			$data["thumbnail"] = is_file(self::generatePathByQid($this->qid)."/thumbnail.jpg");
			return $data;
 		}
	}