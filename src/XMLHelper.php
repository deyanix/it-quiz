<?php
	namespace Quiz;

	use DOMElement;

	class XMLHelper {
		private $element;

		public function __construct(DOMElement $element) {
			$this->element = $element;
		}

		public function getElement() {
			return $this->element;
		}

		public function findSingleElement(string $tag): ?DOMElement {
			$nodeList = $this->element->getElementsByTagName($tag);
			if ($nodeList->count() <= 0) {
				return null;
			}
			return $nodeList->item(0);
		}

		public function findSingleElementValue(string $tag, string $type = null) {
			$element = self::findSingleElement($tag);
			if (empty($element)) {
				return null;
			}
			if (!empty($type)) {
				$value = $element->textContent;
				settype($value, $type);
				return $value;
			} else {
				return $element->textContent;
			}
		}

	}