<?php
	namespace Quiz;

	class GameQuiz {
		protected static function getGameDirectoryPath(): string {
			return PROJECT_DIR."/cache/game";
		}

		protected static function getGameFilePathByGame(GameQuiz $game): string {
			return self::getGameFilePathByToken($game->getToken());
		}

		protected static function getGameFilePathByToken(string $token): string {
			return self::getGameDirectoryPath()."/$token.qiz";
		}

		protected static function prepareGameDirectory(): bool {
			if (!is_dir(self::getGameDirectoryPath())) {
				mkdir(self::getGameDirectoryPath(), 0777, true);
				return false;
			}
			return true;
		}

		protected static function existsToken(string $token): bool {
			if (!self::prepareGameDirectory()) {
				return false;
			}
			$files = scandir(self::getGameDirectoryPath());
			foreach ($files as $filename) {
				if (pathinfo($filename, PATHINFO_EXTENSION) === "qiz") {
					if (pathinfo($filename, PATHINFO_FILENAME) === $token) {
						return true;
					}
				}
			}
			return false;
		}

		protected static function generateToken(): string {
			do {
				$token = substr(str_shuffle(MD5(microtime())), 0, 16);
			} while(self::existsToken($token));
			return $token;
		}

		protected static function nativeStoreGame(GameQuiz $game): bool {
			self::prepareGameDirectory();
			if (self::existsToken($game->getToken())) {
				return false;
			}
			file_put_contents(self::getGameFilePathByGame($game), json_encode(self::serializeGame($game)));
			return true;
		}

		protected static function nativeLoadGame(string $token): ?GameQuiz {
			if (!self::prepareGameDirectory() || !self::existsToken($token)) {
				return null;
			}
			return self::unserializeGame(json_decode(file_get_contents(self::getGameFilePathByToken($token)), true));
		}

		public static function deleteGame(string $token) {
			$path = self::getGameFilePathByToken($token);
			unlink($path);
		}

		public static function serializeGame(GameQuiz $game) {
			$json["quiz"] = $game->getQuiz()->getQid();
			$json["token"] = $game->getToken();
			$json["startTime"] = $game->getStartTime();
			$json["questions"] = [];
			foreach ($game->getGameQuestions() as $question) {
				if ($question instanceof GameQuestion) {
					$jsonQuestion["qid"] = $question->getQuestion()->getId();
					foreach ($question->getAnswers() as $gid => $answer) {
						$jsonQuestion["answers"][$gid] = $answer->getId();
					}
					$json["questions"][$question->getGid()] = $jsonQuestion;
				}
			}
			return $json;
		}

		public static function unserializeGame(array $data): GameQuiz {
			$quiz = Quiz::loadQuiz($data["quiz"]);
			$game = new GameQuiz($quiz, $data["token"]);
			$game->setStartTime($data["startTime"]);
			foreach ($data["questions"] as $gid => $question) {
				$gameQuestion = new GameQuestion($gid, $quiz->getQuestion($question["qid"]));
				foreach ($question["answers"] as $gid => $answer) {
					$gameQuestion->addAnswer($gid, $gameQuestion->getQuestion()->getAnswer($answer));
				}
				$game->addGameQuestion($gameQuestion);
			}
			return $game;
		}

		public static function createGame(Quiz $quiz): GameQuiz {
			$game = new GameQuiz($quiz, self::generateToken());
			$game->setStartTime(round(microtime(true) * 1000));
			$questions = $quiz->getQuestions();
			shuffle($questions);
			for ($i = 0; $i < $quiz->getQuestionsAmount(); $i++) {
				$question = $questions[$i];
				if ($question instanceof QuizQuestion) {
					$gameQuestion = new GameQuestion($i+1, $question);
					$answers = $question->getAnswers();
					if ($question->getOrder() === "random") {
						shuffle($answers);
					}
					foreach ($answers as $index => $answer) {
						$gameQuestion->addAnswer($index+1, $answer);
					}
					$game->addGameQuestion($gameQuestion);
				}
			}
			self::nativeStoreGame($game);
			return $game;
		}

		public static function loadGame(string $token): ?GameQuiz {
			return self::nativeLoadGame($token);
		}

		private $quiz;
		private $token;
		private $startTime;
		private $gameQuestions = [];

		private function __construct(Quiz $quiz, string $token) {
			$this->quiz = $quiz;
			$this->token = $token;
		}

		public function getQuiz(): Quiz {
			return $this->quiz;
		}

		public function getStartTime(): int {
			return $this->startTime;
		}

		public function setStartTime(int $startTime): void {
			$this->startTime = $startTime;
		}

		public function addGameQuestion(GameQuestion $question): void {
			$this->gameQuestions[] = $question;
		}

		public function getGameQuestions(): array {
			return $this->gameQuestions;
		}

		public function findGameQuestionByGid(int $gid): ?GameQuestion {
			foreach ($this->gameQuestions as $gameQuestion) {
				if ($gameQuestion->getGid() === $gid) {
					return $gameQuestion;
				}
			}
			return null;
		}

		public function setGameQuestions($gameQuestions): void {
			$this->gameQuestions = $gameQuestions;
		}

		public function getToken(): string {
			return $this->token;
		}

		public function check(): int {
			$points = 0;
			foreach ($this->gameQuestions as $question) {
				if ($question instanceof GameQuestion) {
					$correctChoice = false;
					if ($question->getQuestion()->getType() === "multiple") {
						if (is_array($question->getChoice())) {
							$correctChoice = true;
							foreach ($question->getAnswers() as $gid => $answer) {
								if ($answer instanceof QuizAnswer) {
									if ($answer->isCorrect() !== in_array($gid, $question->getChoice())) {
										$correctChoice = false;
										break;
									}
								} else {
									$correctChoice = false;
									break;
								}
							}
						}
					} else {
						if (is_int($question->getChoice())) {
							foreach ($question->getAnswers() as $gid => $answer) {
								if ($answer instanceof QuizAnswer) {
									if ($answer->isCorrect() && $gid === $question->getChoice()) {
										$correctChoice = true;
									}
								}
							}
						}
					}

					if ($correctChoice) {
						$points++;
					}
				}
			}
			return $points;
		}

		public function prepare(): array {
			$data["qid"] = $this->quiz->getQid();
			$data["name"] = $this->quiz->getName();
			$data["startTime"] = $this->startTime;
			$data["time"] = $this->quiz->getTime();
			$data["questionsAmount"] = $this->quiz->getQuestionsAmount();
 			$data["token"] = $this->token;
 			foreach ($this->gameQuestions as $gameQuestion) {
 				if ($gameQuestion instanceof GameQuestion) {
 					$question = $gameQuestion->getQuestion();
 					$dataQuestion["content"] = $question->getContent();
 					$dataQuestion["type"] = $question->getType();
 					if (!empty($question->getImage())) {
						$dataQuestion["image"] = $question->getImage();
					}
 					foreach ($gameQuestion->getAnswers() as $gid => $answer) {
						$dataQuestion["answers"][$gid] = $answer->getContent();
					}
 					$data["questions"][$gameQuestion->getGid()] = $dataQuestion;
 					unset($dataQuestion);
				}
			}
 			return $data;
		}
	}