<?php
	namespace Quiz;

	class QuizAnswer {
		public static function loadByElement(int $id, \DOMElement $answerNode) {
			$answerObject = new QuizAnswer($id);

			$answerObject->setContent($answerNode->textContent);
			$answerObject->setCorrect($answerNode->getAttribute('correct') ?? false);
			return $answerObject;
		}

		private $id;
		private $content;
		private $correct;

		protected function __construct(int $id) {
			$this->id = $id;
		}

		public function getId(): int {
			return $this->id;
		}

		public function getContent(): ?string {
			return $this->content;
		}

		public function setContent($content): void {
			$this->content = $content;
		}

		public function isCorrect(): ?bool {
			return $this->correct;
		}

		public function setCorrect($correct): void {
			$this->correct = $correct;
		}
	}