<?php
	namespace Quiz;


	class GameQuestion {
		private $gid;
		private $question;
		private $answers = [];
		private $choice = [];

		public function __construct(int $gid, QuizQuestion $question) {
			$this->gid = $gid;
			$this->question = $question;
		}

		public function getGid(): int {
			return $this->gid;
		}

		public function setGid(int $gid): void {
			$this->gid = $gid;
		}

		public function getQuestion(): QuizQuestion {
			return $this->question;
		}

		public function getChoice() {
			return $this->choice;
		}

		public function setChoice($choice): void {
			$this->choice = $choice;
		}

		public function addAnswer(int $gid, QuizAnswer $answer): void {
			$this->answers[$gid] = $answer;
		}

		public function getAnswers(): array {
			return $this->answers;
		}

		public function findAnswerByGid(int $gid): ?QuizAnswer {
			foreach ($this->answers as $answer) {
				if ($answer->getGid() === $gid) {
					return $answer;
				}
			}
			return null;
		}

		public function setAnswers(array $answers): void {
			$this->answers = $answers;
		}
	}