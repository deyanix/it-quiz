<?php
	namespace Quiz;

	class QuizQuestion {
		public static function loadByElement(int $id, \DOMElement $questionNode): QuizQuestion {
			$questionObject = new QuizQuestion($id);
			$questionObject->setType($questionNode->hasAttribute('type') ? $questionNode->getAttribute('type') : 'single');
			$questionObject->setOrder($questionNode->hasAttribute('order') ? $questionNode->getAttribute('order') : 'random');

			$questionHelper = new XMLHelper($questionNode);
			$questionObject->setContent($questionHelper->findSingleElementValue('content'));
			$questionObject->setImage($questionHelper->findSingleElementValue('image'));


			foreach ($questionNode->getElementsByTagName('answer') as $index => $answerNode) {
				if ($answerNode instanceof \DOMElement) {
					$questionObject->addAnswer(QuizAnswer::loadByElement($index+1, $answerNode));
				}
			}

			return $questionObject;
		}

		private $id;
		private $content;
		private $type;
		private $order;
		private $image;
		private $answers = [];

		protected function __construct(int $id) {
			$this->id = $id;
		}

		public function getId(): int {
			return $this->id;
		}

		public function getContent(): ?string {
			return $this->content;
		}

		public function setContent(?string $content): void {
			$this->content = $content;
		}

		public function getType(): ?string {
			return $this->type;
		}

		public function setType(?string $type): void {
			$this->type = $type;
		}

		public function getOrder(): ?string {
			return $this->order;
		}

		public function setOrder(?string $order): void {
			$this->order = $order;
		}

		public function getImage(): ?string {
			return $this->image;
		}

		public function setImage(?string $image): void {
			$this->image = $image;
		}

		public function addAnswer(QuizAnswer $answer) {
			$this->answers[] = $answer;
		}

		public function getAnswers(): array {
			return $this->answers;
		}

		public function getAnswer(int $id): QuizAnswer {
			foreach ($this->answers as $answer) {
				if ($answer->getId() === $id) {
					return $answer;
				}
			}
			return null;
		}

		public function setAnswers(array $answers): void {
			$this->answers = $answers;
		}
	}